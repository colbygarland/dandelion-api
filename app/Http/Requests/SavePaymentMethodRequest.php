<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SavePaymentMethodRequest
 * @package App\Http\Requests
 * @property string paymentMethod
 * @property string address
 * @property string address2
 * @property string city
 * @property string postal_code
 * @property string phone
 * @property string province
 */
class SavePaymentMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paymentMethod' => ['required'],
            'address' => ['required'],
            'address2' => ['nullable'],
            'postal_code' => ['required'],
            'city' => ['required'],
            'province' => ['required'],
            'phone' => ['nullable'],
            'is_local' => ['nullable'],
            'is_gift' => ['nullable'],
            'gift_message' => ['nullable'],
            'shipping_fname' => ['nullable'],
            'shipping_lname' => ['nullable'],
            'shipping_address' => ['nullable'],
            'shipping_address2' => ['nullable'],
            'shipping_city' => ['nullable'],
            'shipping_province' => ['nullable'],
            'shipping_postal' => ['nullable'],
        ];
    }
}
