<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class PrivacyPolicyController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(){
        return view('pages.privacy-policy');
    }
}
