<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Laravel\Cashier\Subscription;
use DB;

class SubscribeController extends Controller
{
    /**
     * Show the subscribe page.
     *
     * @return Application|Factory|View
     */
    public function index(){
        return view('pages.subscribe', [
            'subscriptions' => Subscription::query()
                ->active()
                ->count(),
            'count_single_boxes' => DB::table('single_boxes')
                ->latest()
                ->value('count'),
        ]);
    }
}
