<?php

namespace App\Http\Controllers\Pages;

use App\ContactEntry;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Mail\ContactEntry as MailContactEntry;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Mail;

class ContactController extends Controller
{
    /**
     * Show the contact page.
     *
     * @return Application|Factory|View
     */
    public function index(){
        return view('pages.contact', [
            'subjects' => ContactEntry::SUBJECTS,
        ]);
    }

    /**
     * Save a new contact entry.
     *
     * @param ContactRequest $request
     * @return RedirectResponse
     */
    public function form(ContactRequest $request){
        $entry = new ContactEntry();
        $entry->name = $request->name;
        $entry->email = $request->email;
        $entry->subject = $request->subject;
        $entry->comments = $request->comments;
        $entry->ip_address = $request->ip();
        $entry->save();

        // Send the email 
        Mail::to(config('mail.email'))
            ->cc('colbygarland@gmail.com')
            ->send(new MailContactEntry(
                $request->name, 
                $request->email, 
                'New Submission from dandelionontheprairie.ca', 
                $request->comments,
                $request->subject
            ));

        return back()->with('success', 'Your submission has been received.');
    }

}
