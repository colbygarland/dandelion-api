<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Show the home page.
     *
     * @return Renderable
     */
    public function index(){
        return view('pages.index');
    }
}
