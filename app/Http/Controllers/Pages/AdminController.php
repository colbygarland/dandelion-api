<?php

namespace App\Http\Controllers\Pages;

use App\ContactEntry;
use App\User;
use App\Order;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use DB;
use App\Http\Requests\FloretFormRequest;
use Artisan;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }

    /**
     * @return Application|Factory|View
     */
    public function users(){
        return view('pages.admin.users', [
            'head' => [
                'name',
                'email',
                'phone',
                'address',
                'address2',
                'city',
                'province',
                'postal_code',
                'subscribed',
                'is_local',
            ],
            'users' => User::orderBy('created_at', 'desc')->get(),
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function contactFormEntries(){
        return view('pages.admin.contacts', [
            'head' => [
                'name',
                'email',
                'subject',
                'comments',
                'created_at',
            ],
            'entries' => ContactEntry::unread()->orderBy('created_at', 'desc')->get(),
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function floretSettings(){
        $numberOfBoxes = DB::table('single_boxes')
            ->latest()
            ->value('count');
        return view('pages.admin.floret', [
            'numberOfBoxes' => $numberOfBoxes,
        ]);
    }

    public function floretForm(FloretFormRequest $request){
        $number = $request->number;
        Artisan::call("update:boxes $number");

        return back()->with('success', "The boxes count has been updated. The new count is: $number");
    }

    public function orders(){
        $orders = Order::orderBy('created_at', 'desc')->get();
        return view('pages.admin.orders', [
            'orders' => $orders,
        ]);
    }

}
