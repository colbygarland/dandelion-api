<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class TermsOfServiceController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(){
        return view('pages.terms-of-service');
    }
}
