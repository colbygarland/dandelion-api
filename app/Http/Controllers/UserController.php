<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserGet;
use App\User;
use App\Survey;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    /**
     * Check and see if a user exists by email.
     *
     * @param UserGet $request
     * @return JsonResponse|bool
     */
    public function get(UserGet $request){
        $user = User::email($request->email)->first();

        if(is_null($user)){
            return response()->json([
                'user' => null,
                'message' => 'An account with this email does not exist. You will now be redirected to create an account.',
                'exists' => false
            ]);
        } else {
            return response()->json([
                'message' => 'An account with this email already exists. You will now be redirected to log in with that email.',
                'user' => $user,
                'exists' => true
            ]);
        }
    }

    public function survey(Request $request){
        $user = Auth::user() ?? null;
        $answer = $request->answer;
        $other  = $request->other;

        $survey = new Survey();
        $survey->user_id = $user->id ?? 1;
        $survey->answer = $answer;
        $survey->other = $other;
        $survey->save();

        return response()->json([
            'message' => 'Response successfully saved. Thank you for your time.',
        ]);
    }

}
