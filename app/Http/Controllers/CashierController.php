<?php

namespace App\Http\Controllers;

use App\Http\Requests\SavePaymentMethodRequest;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Laravel\Cashier\Subscription;
use View;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Order;
use DB;
use Log;
use App\Mail\SingleBoxPurchase;
use Mail;
use App;

class CashierController extends Controller
{

    const REGULAR_SINGLE_BOX_PRICE = 49.99;
    const SALE_DISCOUNT = 0.10; // 10% off 
    // If you want a sale, set a Carbon date here in the future 
    // Once set, the sale price will be on until that date passes
    // example: Carbon::parse('May 31 23:59:59', 'America/Edmonton') 
    public static function isFloretOnSale(): bool {
        $now = Carbon::now()->tz('America/Edmonton');
        $onSaleUntil = Carbon::parse('May 31 23:59:59', 'America/Edmonton');
        return $now->lt($onSaleUntil);
    }

    public static function getSalesPrice($rounded = false): float {
        $price = self::REGULAR_SINGLE_BOX_PRICE - (self::SALE_DISCOUNT * self::REGULAR_SINGLE_BOX_PRICE);
        if($rounded) return round($price, 2);
        else return $price;
    }
    
    /**
     * @param Request $request
     * @return mixed
     */
    public function billingPortal(Request $request){
        /** @var User $user */
        $user = Auth::user();
        if(is_null($user)){
            return redirect()->route('login');
        }
        $stripeCustomer = $user->createOrGetStripeCustomer();
        return $request->user()->redirectToBillingPortal();
    }

    /**
     * @return Application|Factory|View
     */
    public function setupIntent(){
        return view('pages.billing.buy', [
        ]);
    }

    /**
     * @param SavePaymentMethodRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Laravel\Cashier\Exceptions\PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     */
    public static function saveSubscription(SavePaymentMethodRequest $request){
        // Check and ensure we are not at the limit yet
        if(Subscription::query()->active()->count() >= config('app.max_subscriptions')){
            return response()->json(['message' => 'We are sorry, we have sold out. Please email dandelionontheprairie@gmail.com to get on our waitlist.'], Response::HTTP_BAD_REQUEST);
        }

        /** @var User $user */
        $user = Auth::user();
        $paymentMethod = $request->paymentMethod;
        
        // Save the payment info in Stripe
        self::savePaymentMethod($request, $user, $paymentMethod, 'field');

        // Start the subscription on the first day of the next month
        // Offset the time to account for UTC
        $anchor = Carbon::parse('first day of next month')->endOfDay();

        // Create a new subscribtion
        $user->newSubscription('Monthly', config('app.stripe_subscription_id'))
            ->noProrate()
            ->anchorBillingCycleOn($anchor)
            ->create($paymentMethod);

        $user->sendEmail();

        $response = [
            'message' => 'Payment method successfully saved. Subscription created.',
            'redirect' => route('thank-you'),
        ];
        return response()->json($response);
    }

    /**
     * Save the customer's payment methods into Stripe.
     *
     * @param SavePaymentMethodRequest $request
     * @param $user
     * @return Order
     */
    private static function savePaymentMethod(SavePaymentMethodRequest $request, $user, $paymentMethod, string $orderType): Order {
        // Save the user's address
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->country = 'CA';
        $user->postal_code = $request->postal_code;
        $user->city = $request->city;
        $user->province = $request->province;
        $user->phone = $request->phone;
        $user->is_local = $request->is_local;
        $user->save();

        $order = new Order();
        $order->type = $orderType;
        $order->user_id = $user->id;
        $order->email = $user->email;

        $order->billing_name =  $user->name;
        $order->billing_address = $request->address;
        $order->billing_address2 = $request->address2;
        $order->billing_country = 'CA';
        $order->billing_city = $request->city;
        $order->billing_province = $request->province;
        $order->billing_postal = $request->postal_code;
        $order->billing_phone = $request->phone;

        // Set the shipping address to billing initially
        $order->shipping_name =  $user->name;
        $order->shipping_address = $request->address;
        $order->shipping_address2 = $request->address2;
        $order->shipping_country = 'CA';
        $order->shipping_city = $request->city;
        $order->shipping_province = $request->province;
        $order->shipping_postal = $request->postal_code;

        // Check and see if we have a separate shipping address
        $shipping = [];
        if($request->shipping_address){
            $shipping = [
                'name' => $request->shipping_fname . ' ' . $request->shipping_lname,
                'address' => [
                    'line1' => $request->shipping_address,
                    'line2' => $request->shipping_address2,
                    'city' => $request->shipping_city,
                    'country' => 'CA',
                    'postal_code' => $request->shipping_postal,
                    'state' => $request->shipping_province,
                ],
            ];

            $order->shipping_name =  $request->shipping_fname . ' ' . $request->shipping_lname;
            $order->shipping_address = $request->shipping_address;
            $order->shipping_address2 = $request->shipping_address2;
            $order->shipping_country = 'CA';
            $order->shipping_city = $request->shipping_city;
            $order->shipping_province = $request->shipping_province;
            $order->shipping_postal = $request->shipping_postal;
        }

        $order->save();

        // Create as a Stripe user
        $user->createOrGetStripeCustomer([
            'name' => $user->name,
            'address' => [
                'line1' => $request->address,
                'line2' => $request->address2,
                'city' => $request->city,
                'postal_code' => $request->postal_code,
                'state' => $request->province,
                'country' => 'CA',
            ],
            'shipping' => $shipping,
            'email' => $user->email,
            'phone' => $user->phone,
            'metadata' => [
                'gift_note' => $request->gift_message,
            ]
        ]);

        // Save the payment method
        $user->addPaymentMethod($paymentMethod);

        // Set as the default
        $user->updateDefaultPaymentMethod($paymentMethod);

        return $order;
    }

    public static function purchaseSingleBox(SavePaymentMethodRequest $request){
       if(self::singleBoxesOutOfStock()){
           return response()->json([
               'message' => 'We are sorry, we are currently sold out of single boxes.',
               'error' => 'sold_out',
           ], Response::HTTP_BAD_REQUEST);
       }

       $user = Auth::user();
       $paymentMethod = $request->paymentMethod;

       $order = self::savePaymentMethod($request, $user, $paymentMethod, 'floret');

       // Purchase the box
       $quantity = $request->quantity;

       // Check for sales
       if(self::isFloretOnSale()){
         $price = ((self::getSalesPrice() * $quantity) * 100);
         $tax = (self::getSalesPrice() * $quantity) * 0.05 * 100;
         $totalPrice = ceil($price + $tax);
       } else {
         $price = ((self::REGULAR_SINGLE_BOX_PRICE * $quantity) * 100);
         $tax = (self::REGULAR_SINGLE_BOX_PRICE * $quantity) * 0.05 * 100;
         $totalPrice = ceil($price + $tax);
       }
     
       $user->invoiceFor('Dandelion Floret Box', $totalPrice);

       // Gift stuff
       $isGift = $request->is_gift;
       $giftMessage = $request->gift_message ? $request->gift_message : '';
       $order->gift_note = $giftMessage;
       $order->save();

       // Decrement the count of single boxes
       $currentCount = DB::table('single_boxes')->latest()->value('count') - $quantity;
       DB::table('single_boxes')
            ->insert([
                'count' => $currentCount,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        ]);

        $env = App::environment();
        Log::alert("[{$env}]\n{$user->email} just bought $quantity Floret Box(s) :tada: \nWe now have $currentCount single boxes remaining.");
        Mail::to(config('mail.email'))
            ->cc('colbygarland@gmail.com')
            ->send(new SingleBoxPurchase($user->email, $isGift, $giftMessage, $quantity));

        $user->sendEmail();

       $response = [
            'message' => 'Payment successful. Floret box(es) purchased.',
            'redirect' => route('thank-you-single'),
        ];
        return response()->json($response);
    }

    public static function getSingleBoxCount(){
        return DB::table('single_boxes')
        ->latest()
        ->value('count');
    }

    private static function singleBoxesOutOfStock(){
        // Ensure we are not sold out
        $count = self::getSingleBoxCount();
        return $count < 1;
    }

    public function viewPurchaseSingleBox(){
        return view('pages.billing.purchase-single-box');
    }

    /**
     * @return Application|Factory|\Illuminate\View\View
     */
    public function thankYouPage(){
        return view('pages.billing.thank-you');
    }

    public function thankYouPageSingle(){
        return view('pages.billing.thank-you-single');
    }

}
