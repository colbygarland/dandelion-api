<?php

namespace App\Http\Controllers;

use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use Symfony\Component\HttpFoundation\Response;
use App\User;

class WebhookController extends CashierController
{
    protected function successMethod($parameters = [])
    {
        return new Response('Webhook Handled', 200);
    }

    public function handleCustomerUpdated($payload){
        $userData = $payload['data']['object'];
        $user = User::where('stripe_id', $userData['id'])->first();
        if(is_null($user)) return;

        $user->updateDefaultPaymentMethodFromStripe();

        $address = $userData['address'];
        if(is_null($address)) return $this->successMethod();
        
        $user->address = $address['line1'];
        $user->address2 = $address['line2'];
        $user->city = $address['city'];
        $user->country = $address['country'];
        $user->postal_code = $address['postal_code'];
        $user->province = $address['state'];
        $user->save();

        return $this->successMethod();
    }

    public function handleCustomerSubscriptionDeleted($payload){
        $stripeId = $payload['data']['object']['customer'];
        $user = User::where('stripe_id', $stripeId)->first();
        if(is_null($user)) return;

        $user->subscriptions->filter(function ($subscription) use ($payload) {
            return $subscription->stripe_id === $payload['data']['object']['id'];
        })->each(function ($subscription) {
            $subscription->markAsCancelled();
        });

        \Log::alert(":x: :x: {$user->email} just cancelled their subscription.");

        return $this->successMethod();
    }

}
