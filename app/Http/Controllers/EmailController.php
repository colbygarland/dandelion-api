<?php

namespace App\Http\Controllers;

use App\Mail\ContactEntry;
use App\Mail\NewPurchase;
use App\Mail\SurveyInfo;
use App\Survey;
use Carbon\Carbon;

class EmailController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }

    public function entry(){
        return new ContactEntry('Colby Garland', 'colbygarland@gmail.com', 'Hello there', 'Hi I have a question');
    }

    public function newPurchase() {
        return new NewPurchase(1);
    }

    public static function surveyResults() {
        $surveys = Survey::selectRaw('count(answer) as count, answer')
            ->groupBy('answer')
            ->where('created_at', '>=', Carbon::now()->subWeek())
            ->where('created_at', '<=', Carbon::now())
            ->get();
        if ($surveys->isEmpty()) return null;
        else return new SurveyInfo($surveys);
    }

}
