<?php

namespace App\Http\Controllers;

use Auth;

class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('pages.account', [
            'user' => Auth::user(),
        ]);
    }
}
