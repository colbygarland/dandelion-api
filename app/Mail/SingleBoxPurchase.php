<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SingleBoxPurchase extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $isGift;
    public $giftMessage;
    public $quantity;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $email, bool $isGift, string $giftMessage, int $quantity)
    {
        $this->email = $email;
        $this->isGift = $isGift;
        $this->giftMessage = $giftMessage;
        $this->quantity = $quantity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.purchase');
    }
}
