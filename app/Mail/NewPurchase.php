<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPurchase extends Mailable
{
    use Queueable, SerializesModels;

    private int $orderNum;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(int $orderNum)
    {
        $this->orderNum = $orderNum;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('💛 Your Dandelion order #' . $this->orderNum)
            ->markdown('emails.new-customer');
    }
}
