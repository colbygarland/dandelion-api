<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SurveyInfo extends Mailable
{
    use Queueable, SerializesModels;

    private $surveys;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($surveys)
    {
        $this->surveys = $surveys;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.survey-results', [
            'surveys' => $this->surveys
        ]);
    }
}
