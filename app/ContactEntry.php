<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactEntry
 *
 * @package App
 * @property int id
 * @property string name
 * @property string email
 * @property string subject
 * @property string comments
 * @property string ip_address
 * @property Carbon created_at
 * @property Carbon updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactEntry whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactEntry extends Model
{
    protected $dates = [
        'created_At' => Carbon::class,
        'updated_at' => Carbon::class,
    ];

    const SUBJECTS = [
        'General',
        'Subscription Renewal',
        'Subscription Cancellation',
        'Become a Vendor',
        'Referral for a Canadian Company',
        'Other',
    ];

    public function scopeUnread($query){
        return $query->where('read', false);
    }

}
