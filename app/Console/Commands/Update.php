<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update {branch_name=master} {--npm}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to pull git, update website, run npm, update sitemap, etc.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $branch = $this->argument('branch_name');
        $npm = $this->option('npm');

        $this->info("\nPutting website into maintenance mode...");
        exec('php artisan down');

        $this->info("Updating from git, on branch $branch...");

        // Refresh the git repo
        exec('git fetch --all') . "\n";

        // Reset our head to the latest commit on master
        $headMessage = exec("git reset --hard origin/$branch");
        $this->info($headMessage);

        // Remove any untracked files to prevent a merge conflict
        echo exec('git clean -dfq') . "\n";

        $this->info('Updating composer...');
        echo exec('composer install -q');

        if($npm){
            $this->info('Running npm run prod..');
            echo exec('npm run prod') . "\n";
        }

        $this->info('Running migrations...');
        echo exec('php artisan migrate --force');
        
        $this->info("\n\nClearing application caches...");
        exec('php artisan cache:clear');
        exec('php artisan config:clear');
        exec('php artisan view:clear');

        $this->info("Caching services...");
        Artisan::call('config:cache');
        Artisan::call('view:cache');
        Artisan::call('route:cache');
        echo Artisan::output();

        $this->info('Remote successfully updated!');

        $this->info('Restoring from maintenance mode...');
        exec('php artisan up');

        return 0;
    }
}
