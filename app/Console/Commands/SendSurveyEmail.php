<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\EmailController;
use Mail;

class SendSurveyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:sendSurvey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send out the survey report.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $email = EmailController::surveyResults();
        if(is_null($email)){
            return 0;
        }

        Mail::to(config('mail.email'))
            ->cc('colbygarland@gmail.com')
            ->send($email);
        return 0;
    }
}
