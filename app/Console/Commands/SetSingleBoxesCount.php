<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;

class SetSingleBoxesCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:boxes {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the count of the individual boxes we have for sale.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = $this->argument('count');

        $rowsAffected = DB::table('single_boxes')
            ->insert([
                'count' => $count,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        ]);

        if($rowsAffected != 1){
            $this->error('Something went wrong.');
            return 1;
        }

        $this->info("Successfully updated the count of boxes for sale. Rows affected: $rowsAffected");
        return 0;
    }
}
