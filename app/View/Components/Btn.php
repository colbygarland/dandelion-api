<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Btn extends Component
{
    public $href;
    public $type;
    public $id;
    public $class;
    public $disabled;

    /**
     * Create a new component instance.
     *
     * @param $type
     * @param string $href
     * @param string $id
     * @param string $class
     */
    public function __construct($type, $href='', $id='', $class='', $disabled = false)
    {
        $this->type = $type;
        $this->href = $href;
        $this->id = $id;
        $this->class = $class;
        $this->disabled = $disabled;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.btn');
    }
}
