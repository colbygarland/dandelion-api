<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PhotoBackground extends Component
{

    public $photo;
    public $slant;
    public $class;

    /**
     * Create a new component instance.
     *
     * @param $photo
     * @param null $slant
     * @param null $class
     */
    public function __construct($photo, $slant=null, $class=null)
    {
        $this->photo = $photo;
        $this->slant = $slant;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.photo-background');
    }
}
