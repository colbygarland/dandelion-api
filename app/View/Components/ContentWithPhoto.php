<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ContentWithPhoto extends Component
{
    public $photo;

    /**
     * Create a new component instance.
     *
     * @param $photo
     */
    public function __construct($photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.content-with-photo');
    }
}
