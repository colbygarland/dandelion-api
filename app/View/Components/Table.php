<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Table extends Component
{
    public $head;
    public $body;

    /**
     * Create a new component instance.
     *
     * @param $head
     * @param $body
     */
    public function __construct($head, $body)
    {
        $h = explode(',', $head);
        $this->head = $h;
        $this->body = $body;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.table');
    }
}
