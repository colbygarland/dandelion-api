<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{

    public $name;
    public $required;
    public $placeholder;
    public $value;
    public $type;
    public $id;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $required
     * @param $placeholder
     * @param $value
     * @param $type
     * @param $id
     */
    public function __construct($name, $required=false, $placeholder='', $value='', $type='text', $id='')
    {
        $this->name = $name;
        $this->required = $required;
        $this->placeholder = $placeholder;
        $this->value = $value;
        $this->type = $type;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
