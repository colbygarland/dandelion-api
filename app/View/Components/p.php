<?php

namespace App\View\Components;

use Illuminate\View\Component;

class p extends Component
{

    public $class;

    /**
     * Create a new component instance.
     *
     * @param string $class
     */
    public function __construct($class='')
    {
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.p');
    }
}
