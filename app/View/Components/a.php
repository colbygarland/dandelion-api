<?php

namespace App\View\Components;

use Illuminate\View\Component;

class a extends Component
{
    public $href;
    public $class;
    public $target;

    /**
     * Create a new component instance.
     *
     * @param $href
     * @param string $target
     * @param string $class
     */
    public function __construct($href, $target='', $class='')
    {
        $this->href = $href;
        $this->target = $target;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.a');
    }
}
