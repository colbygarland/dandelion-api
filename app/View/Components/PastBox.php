<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PastBox extends Component
{

    public string $img;
    public string $title;
    public bool $visible;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $img, string $title, bool $visible = true)
    {
        $this->img = $img;
        $this->title = $title;
        $this->visible = $visible;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.past-box');
    }
}
