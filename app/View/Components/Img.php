<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Img extends Component
{
    public $src;
    public $alt;

    /**
     * Create a new component instance.
     *
     * @param $src
     * @param string $alt
     */
    public function __construct($src, $alt='')
    {
        $this->src = $src;
        $this->alt = $alt;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.img');
    }
}
