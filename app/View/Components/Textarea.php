<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Textarea extends Component
{

    public $name;
    public $required;
    public $placeholder;
    public $value;
    public $id;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param bool $required
     * @param string $placeholder
     * @param string $value
     * @param string $id
     */
    public function __construct($name, $required=false, $placeholder='', $value='', $id='')
    {
        $this->name = $name;
        $this->required = $required;
        $this->placeholder = $placeholder;
        $this->value = $value;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.textarea');
    }
}
