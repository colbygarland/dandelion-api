<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FloatingAlert extends Component
{
    public string $title;
    public string $desc;
    public string $link;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $title, string $desc, string $link)
    {
        $this->title = $title;
        $this->desc = $desc;
        $this->link = $link;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.floating-alert');
    }
}
