<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Content extends Component
{

    public $class;
    public $id;

    /**
     * Create a new component instance.
     *
     * @param string $class
     */
    public function __construct($class='', $id='')
    {
        $this->class = $class;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.content');
    }
}
