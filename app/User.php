<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Mail;
use App\Mail\NewPurchase;
use App\Order;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'subscribed',
    ];

    /**
     * @param $query
     * @param string $email
     * @return mixed
     */
    public function scopeEmail($query, string $email){
        return $query->where('email', $email);
    }

    public function getSubscribedAttribute(): bool {
        if($this->subscribed('Monthly')) return true;
        else return false;
    }

    public function getLatestOrder(){
        return Order::where('user_id', $this->id)->latest()->first();
    }

    public function sendEmail() {
        $latestOrder = $this->getLatestOrder();
        Mail::to($this->email)
            ->send(new NewPurchase($latestOrder->id));
    }

}
