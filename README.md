# Dandelion on the Prairie

### Helpful commands

-   `artisan update {branch_name}` - Update the website from the git branch specified
-   `artisan update:boxes {number_of_single_boxes}` - Set the number of single boxes to `number_of_single_boxes`
-   `certbot --force-renewal -d dandelionontheprairie.ca` - Renew the Let's Encrypt cert
-   `npm run prod` - Compile JS for production (also purges Tailwind CSS)
-   Ran in WSL: `sudo /etc/init.d/mysql start` restart mysql locally
