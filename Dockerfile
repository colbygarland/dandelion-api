FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y \
    && apt-get install openssh-client composer php-mbstring unzip -y \
    && composer global require "laravel/envoy"
