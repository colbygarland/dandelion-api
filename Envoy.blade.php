@servers(['web' => 'deployer@dandelionontheprairie.ca'])

@setup
    $repository = 'git@gitlab.com:colbygarland/dandelion-api.git';
    $releases_dir = '/var/www/dev/releases';
    $app_dir = '/var/www/dev';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_setup
    run_migrations
    run_tests
    update_symlink
    build_cache
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1  --branch {{ $branch }} {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_setup')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    npm install -s
    npm run production
@endtask

@task('run_tests')
    echo "Start testing ({{ $release }})"
    cd {{ $new_release_dir }}
    vendor/bin/phpunit --testdox
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    echo "Linking public storage directory"
    ln -nfs {{ $app_dir }}/storage/app/public {{ $new_release_dir }}/public/storage
@endtask

@task('run_migrations')
    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
    echo "Run migrations"
    cd {{ $new_release_dir }}
    php artisan migrate --force
@endtask

@task('build_cache')
    echo 'Building cache'
    cd {{ $new_release_dir }}
    php artisan config:cache
    php artisan view:cache
    php artisan route:cache
@endtask
