<?php

use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Pages\HomeController@index')->name('home');
Route::get('about', 'Pages\AboutController@index')->name('about');
Route::get('subscribe', 'Pages\SubscribeController@index')->name('subscribe');
Route::get('contact', 'Pages\ContactController@index')->name('contact');
Route::get('terms-of-service', 'Pages\TermsOfServiceController@index')->name('terms-of-service');
Route::get('privacy-policy', 'Pages\PrivacyPolicyController@index')->name('privacy-policy');
Route::get('newsletter', 'Pages\NewsletterController@index')->name('newsletter');

Route::get('account', 'AccountController@index')->name('account');

Route::prefix('checkout')->group(function(){
    Route::get('portal', 'CashierController@billingPortal')->name('portal');
    Route::get('subscribe', 'CashierController@setupIntent')->name('buy');
    Route::get('purchase', 'CashierController@viewPurchaseSingleBox')->name('buy_single_box');
    Route::get('thank-you', 'CashierController@thankYouPage')->name('thank-you');
    Route::get('thank-you-purchase', 'CashierController@thankYouPageSingle')->name('thank-you-single');
});

Route::prefix('admin')->middleware(['admin'])->group(function(){
    Route::get('users', 'Pages\AdminController@users');
    Route::get('entries', 'Pages\AdminController@contactFormEntries');
    Route::get('floret', 'Pages\AdminController@floretSettings');
    Route::get('orders', 'Pages\AdminController@orders');
});

Route::prefix('email')->group(function(){
    Route::get('entry', 'EmailController@entry');
    Route::get('new-purchase', 'EmailController@newPurchase');
    Route::get('survey-results', 'EmailController@surveyResults');
});

Route::post('/stripe/webhook', [WebhookController::class, 'handleWebhook']);

Auth::routes();
