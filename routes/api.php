<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('contact', 'Pages\ContactController@form')->name('contact_form');

Route::prefix('users')->group(function(){
    Route::post('get', 'UserController@get');
});

Route::post('survey', 'UserController@survey');

Route::middleware(['auth'])->group(function(){

    Route::post('create_subscription', 'CashierController@saveSubscription');
    Route::post('purchase_single_box', 'CashierController@purchaseSingleBox');
    Route::post('floret', 'Pages\AdminController@floretForm')->name('floret_form');

});
