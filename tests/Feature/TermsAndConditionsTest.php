<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TermsAndConditionsTest extends TestCase
{
    public function testTermsAndConditionsPage()
    {
        $response = $this->get('terms-of-service');

        $response->assertStatus(200);
    }
}
