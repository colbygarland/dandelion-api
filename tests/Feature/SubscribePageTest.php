<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SubscribePageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSubscribePage()
    {
        $response = $this->get('subscribe');

        $response->assertStatus(200);
    }
}
