<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PrivacyPolicyTest extends TestCase
{
    public function testPrivacyPolicyPage()
    {
        $response = $this->get('privacy-policy');

        $response->assertStatus(200);
    }
}
