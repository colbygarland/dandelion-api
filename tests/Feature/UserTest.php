<?php

namespace Tests\Feature;

use App\Http\Controllers\UserController;
use App\Http\Requests\UserGet;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testGet(){
        $request = new UserGet([
            'email' => 'colbygarland@gmail.com',
        ]);

        $response = (new UserController())->get($request);
        $this->assertTrue($response->status() == 200);
        $this->assertTrue($response->original['exists']);
        $this->assertTrue($response->original['user']['email'] == 'colbygarland@gmail.com');

        $request = new UserGet([
            'email' => 'thisemaildoesnotexist@colby.ca',
        ]);

        $response = (new UserController())->get($request);
        $this->assertTrue($response->status() == 200);
        $this->assertFalse($response->original['exists']);
    }
}
