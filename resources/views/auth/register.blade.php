@extends('layouts.auth.app')
@section('title', 'Register')

@section('content')

    <div class="w-full max-w-xs lg:max-w-md prose">
        @if(Request::has('message'))
            <div class="mb-4">
                <x-alert type="info">{{ Request::get('message') }}</x-alert>
            </div>
        @endif
        <form
            class="bg-white shadow-md rounded px-4 lg:px-8 pt-6 pb-8 mb-4"
            method="POST"
            action="{{ route('register') }}@if(Request::has('redirect'))?redirect={{ Request::get('redirect') }}@endif"
        >
            @csrf
            <h2 style="margin-top:0">Register</h2>
            <p>Creating an account allows you to check order and shipping status, change your info, and more.</p>
            <div class="mb-4">
                <x-label>Email Address</x-label>
                @php
                    if(Request::has('email')) $email = Request::get('email');
                    else $email = old('email');
                @endphp
                <x-input id="email" name="email" placeholder="Email address" required value="{{ $email }}" />
                @error('email')
                <p class="text-red-500 text-xs italic mt-2">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-4">
                <x-label>Full Name</x-label>
                <x-input id="name" name="name" placeholder="Full name" required value="{{ old('name') }}" />
                @error('name')
                <p class="text-red-500 text-xs italic mt-2">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <x-label>Password</x-label>
                @include('components.forms.input', [
                    'id' => 'password',
                    'name' => 'password',
                    'required' => true,
                    'type' => 'password',
                ])
                @error('password')
                <p class="mt-2 text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <x-label>Confirm Password</x-label>
                @include('components.forms.input', [
                    'id' => 'password-confirm',
                    'name' => 'password_confirmation',
                    'required' => true,
                    'type' => 'password',
                ])
                @error('password')
                <p class="mt-2 text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="flex items-center justify-between">
                <button class="w-full bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Register
                </button>
            </div>
            <div class="text-center">
                <p class="text-gray-600"><small>Already have an account with {{ config('app.name') }}? <a href="{{ route('login') }}@if(Request::has('redirect'))?redirect={{ Request::get('redirect') }}@endif">Login</a></small></p>
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            &copy;{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>. All rights reserved.
        </p>
    </div>

@endsection
