@extends('layouts.auth.app')
@section('title', 'Change Password')

@section('content')
    <div class="w-full max-w-xs lg:max-w-md prose">
        <form class="bg-white shadow-md rounded px-4 lg:px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('password.update') }}">
            @csrf
            
            <input type="hidden" name="token" value="{{ $token }}">
            <h2 style="margin-top:0" class="mb-4 lg:mb-6 text-2xl lg:text-3xl font-bold">{{ __('Reset Password') }}</h2>
            <div class="mb-4">
                <x-label>Email address</x-label>
                <x-input name="email" required placeholder="Enter your email address" value="{{ $email ?? old('email') }}" />
            </div>
            <div class="mb-4">
                <x-label>Password</x-label>
                @include('components.forms.input', [
                    'id' => 'password',
                    'name' => 'password',
                    'required' => true,
                    'type' => 'password',
                ])
                @error('password')
                <p class="mt-2 text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-4">
                <x-label>Confirm Password</x-label>
                @include('components.forms.input', [
                    'id' => 'password-confirm',
                    'name' => 'password_confirmation',
                    'required' => true,
                    'type' => 'password',
                ])
                @error('password')
                <p class="mt-2 text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-4">
                <button class="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Submit
                </button>
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            &copy;{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>. All rights reserved.
        </p>
    </div>

@endsection
