@extends('layouts.auth.app')
@section('title', 'Reset Password')

@section('content')

    <div class="w-full max-w-xs lg:max-w-md prose">
        <form class="bg-white shadow-md rounded px-4 lg:px-8 pt-6 pb-8 mb-4" method="POST" action="{{ route('password.email') }}">
            @if (session('status'))
                <x-alert type="success">
                    {{ session('status') }}
                </x-alert>
            @endif
            @csrf
            <h2 style="margin-top:0" class="mb-4 lg:mb-6 text-2xl lg:text-3xl font-bold">Reset Password</h2>
            <div class="mb-4">
                <x-label>Email address</x-label>
                <x-input name="email" required placeholder="Enter your email address" />
            </div>
            <div class="mb-4">
                <button class="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Submit
                </button>
            </div>
            <div class="text-center mt-3">
                <p class="text-gray-600"><small>Don't have an account with {{ config('app.name') }}? <a href="{{ route('register') }}">Register</a></small></p>
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            &copy;{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>. All rights reserved.
        </p>
    </div>

@endsection
