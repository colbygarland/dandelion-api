@extends('layouts.auth.app')
@section('title', 'Login')

@section('content')

    <div class="w-full max-w-xs lg:max-w-md prose">
        @if(Request::has('message'))
            <div class="mb-4">
                <x-alert type="info">{{ Request::get('message') }}</x-alert>
            </div>
        @endif
        <form
            class="bg-white shadow-md rounded px-4 lg:px-8 pt-6 pb-8 mb-4"
            method="POST"
            action="{{ route('login') }}@if(Request::has('redirect'))?redirect={{ Request::get('redirect') }}@endif"
        >
            @csrf
            <h2 style="margin-top:0" class="mb-4 lg:mb-6 text-2xl lg:text-3xl font-bold">Login</h2>
            <div class="mb-4">
                @component('components.forms.label')
                    Email Address
                @endcomponent
                @php
                if(Request::has('email')) $email = Request::get('email');
                else $email = old('email');
                @endphp
                @include('components.forms.input', [
                    'id' => 'email',
                    'name' => 'email',
                    'placeholder' => 'Email',
                    'required' => true,
                    'value' => $email,
                ])
                @error('email')
                    <p class="text-red-500 text-xs italic mt-2">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                @component('components.forms.label')
                    Password
                @endcomponent
                @include('components.forms.input', [
                    'id' => 'password',
                    'name' => 'password',
                    'required' => true,
                    'type' => 'password',
                ])
                @error('password')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-6">
                <label class="md:w-2/3 block text-gray-500 font-bold">
                    <input class="mr-2 leading-tight" type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span class="text-sm">
                        Remember me
                    </span>
                </label>
            </div>
            <div class="flex items-center justify-between">
                <button class="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                    Sign In
                </button>
                @if (Route::has('password.request'))
                    <a class="inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800" href="{{ route('password.request') }}">
                        Forgot password?
                    </a>
                @endif
            </div>
            <div class="text-center mt-3">
                <p class="text-gray-600"><small>Don't have an account with {{ config('app.name') }}? <a href="{{ route('register') }}">Register</a></small></p>
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            &copy;{{ date('Y') }} <a href="{{ route('home') }}">{{ config('app.name') }}</a>. All rights reserved.
        </p>
    </div>

@endsection
