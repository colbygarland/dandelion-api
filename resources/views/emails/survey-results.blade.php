@component('mail::message')

Hi! Here are the last week's survey results from <strong>"Where did you hear about us?"</strong>

<em>Keep in mind this survey is currently only shown after somebody makes a purchase.</em>

@foreach($surveys as $survey)
@component('mail::panel')
{{ $survey->answer }} ({{ $survey->count }})
@endcomponent
@endforeach

Thanks!

@endcomponent
