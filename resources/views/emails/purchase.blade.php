@component('mail::message')

Yay! {{ $email }} just bought {{ $quantity }} Floret box(es)!

@if($isGift)
The customer wants this for the gift message:
@component('mail::panel')
{{ $giftMessage }}
@endcomponent
@endif

@component('mail::panel')
The receipt has been sent automatically by Stripe. If the customer did not get it, you can manually send in Stripe.
@endcomponent

@component('mail::button', ['url' => 'https://dashboard.stripe.com/invoices', 'color' => 'primary'])
Go To Stripe
@endcomponent

Thanks!

@endcomponent
