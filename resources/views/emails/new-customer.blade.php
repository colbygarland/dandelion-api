@component('mail::message')

Thank you for supporting our small business! We appreciate it. 💛 

Did you know that all of our products in our boxes are made in Canada, by Canadians, from coast to coast?! 

We love to support big and small businesses, while also trying to support those who make their products ethically, sustainably, or naturally. We hope you enjoy your box as much as we enjoyed putting it together for you! 🎁

Wondering when your credit card will be charged, or when your box will ship out? Check out our Frequently Asked Questions on our website. 👇👇

@component('mail::button', ['url' => route('home') . '#faqs', 'color' => 'primary'])
View FAQs 
@endcomponent

If you have any other questions or concerns, please reach out at <a href="mailto:{{ config('mail.email') }}">{{ config('mail.email') }}</a> we will assist you as soon as possible!

@endcomponent
