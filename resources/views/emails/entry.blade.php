@component('mail::message')

Hi! There is a new message from dandelionontheprairie.ca

@component('mail::panel')
<strong>Name:</strong> {{ $name }}
@endcomponent

@component('mail::panel')
<strong>Email:</strong> <a href="mailto:{{ $email }}">{{ $email }}</a>
@endcomponent

@component('mail::panel')
<strong>Subject:</strong> {{ $contactSubject }}
@endcomponent

@component('mail::panel')
<strong>Message:</strong> {{ $comments }}
@endcomponent

@component('mail::button', ['url' => 'mailto:' . $email . '?subject=RE:' . $subject, 'color' => 'primary'])
Respond To Message 
@endcomponent

Thanks!

@endcomponent