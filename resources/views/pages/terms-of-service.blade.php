@extends('layouts.app')
@section('title', 'Terms Of Service')
@section('description', 'Dandelion on the Prarie.')

@section('content')

    <x-content>
        <x-container>
            <x-h1>Terms of Service</x-h1>

            <x-p>Welcome to {{ config('app.name') }}!</x-p>

            <x-p>These terms and conditions outline the rules and regulations for the use of {{ config('app.name') }}'s Website, located at {{ config('app.url') }}.</x-p>

            <x-p>By accessing this website we assume you accept these terms and conditions. Do not continue to use {{ config('app.name') }} if you do not agree to take all of the terms and conditions stated on this page. Our Terms and Conditions were created with the help of the <x-a href="https://www.termsandconditionsgenerator.com" target="_blank">Terms And Conditions Generator</x-a> and the <x-a href="https://www.privacypolicyonline.com/terms-conditions-generator/" target="_blank">Free Terms & Conditions Generator</x-a>.</x-p>

            <x-p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</x-p>

            <x-h3><strong>Cookies</strong></x-h3>

            <x-p>We employ the use of cookies. By accessing {{ config('app.name') }}, you agreed to use cookies in agreement with the {{ config('app.name') }}'s Privacy Policy.</x-p>

            <x-p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</x-p>

            <x-h3><strong>License</strong></x-h3>

            <x-p>Unless otherwise stated, {{ config('app.name') }} and/or its licensors own the intellectual property rights for all material on {{ config('app.name') }}. All intellectual property rights are reserved. You may access this from {{ config('app.name') }} for your own personal use subjected to restrictions set in these terms and conditions.</x-p>

            <x-p>You must not:</x-p>
            <x-ul>
                <x-li>Republish material from {{ config('app.name') }}</x-li>
                <x-li>Sell, rent or sub-license material from {{ config('app.name') }}</x-li>
                <x-li>Reproduce, duplicate or copy material from {{ config('app.name') }}</x-li>
                <x-li>Redistribute content from {{ config('app.name') }}</x-li>
            </x-ul>

            <x-p>This Agreement shall begin on the date hereof.</x-p>

            <x-p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. {{ config('app.name') }} does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of {{ config('app.name') }},its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, {{ config('app.name') }} shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</x-p>

            <x-p>{{ config('app.name') }} reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</x-p>

            <x-p>You warrant and represent that:</x-p>

            <x-ul>
                <x-li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</x-li>
                <x-li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</x-li>
                <x-li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</x-li>
                <x-li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</x-li>
            </x-ul>

            <x-p>You hereby grant {{ config('app.name') }} a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</x-p>

            <x-h3><strong>Hyperlinking to our Content</strong></x-h3>

            <x-p>The following organizations may link to our Website without prior written approval:</x-p>

            <x-ul>
                <x-li>Government agencies;</x-li>
                <x-li>Search engines;</x-li>
                <x-li>News organizations;</x-li>
                <x-li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</x-li>
                <x-li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</x-li>
            </x-ul>

            <x-p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.</x-p>

            <x-p>We may consider and approve other link requests from the following types of organizations:</x-p>

            <x-ul>
                <x-li>commonly-known consumer and/or business information sources;</x-li>
                <x-li>dot.com community sites;</x-li>
                <x-li>associations or other groups representing charities;</x-li>
                <x-li>online directory distributors;</x-li>
                <x-li>internet portals;</x-li>
                <x-li>accounting, law and consulting firms; and</x-li>
                <x-li>educational institutions and trade associations.</x-li>
            </x-ul>

            <x-p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of {{ config('app.name') }}; and (d) the link is in the context of general resource information.</x-p>

            <x-p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.</x-p>

            <x-p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to {{ config('app.name') }}. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</x-p>

            <x-p>Approved organizations may hyperlink to our Website as follows:</x-p>

            <x-ul>
                <x-li>By use of our corporate name; or</x-li>
                <x-li>By use of the uniform resource locator being linked to; or</x-li>
                <x-li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</x-li>
            </x-ul>

            <x-p>No use of {{ config('app.name') }}'s logo or other artwork will be allowed for linking absent a trademark license agreement.</x-p>

            <x-h3><strong>iFrames</strong></x-h3>

            <x-p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</x-p>

            <x-h3><strong>Content Liability</strong></x-h3>

            <x-p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</x-p>

            <x-h3><strong>Your Privacy</strong></x-h3>

            <x-p>Please read <x-a href="{{ route('privacy-policy') }}">Privacy Policy</x-a></x-p>

            <x-h3><strong>Reservation of Rights</strong></x-h3>

            <x-p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</x-p>

            <x-h3><strong>Removal of links from our website</strong></x-h3>

            <x-p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</x-p>

            <x-p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</x-p>

            <x-h3><strong>Disclaimer</strong></x-h3>

            <x-p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</x-p>

            <x-ul>
                <x-li>limit or exclude our or your liability for death or personal injury;</x-li>
                <x-li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</x-li>
                <x-li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</x-li>
                <x-li>exclude any of our or your liabilities that may not be excluded under applicable law.</x-li>
            </x-ul>

            <x-p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</x-p>

            <x-p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</x-p>
        </x-container>
    </x-content>

@endsection
