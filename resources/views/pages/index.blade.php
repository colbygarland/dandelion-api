@extends('layouts.app')
@section('title', 'Home')
@section('description', 'Dandelion on the Prarie is a monthly subscription box filled with Canadian made products.')

@section('content')

    <div class="relative bg-gray-100 overflow-hidden">
        <div class="max-w-screen-xl mx-auto">
            <div class="relative z-10 pb-8 bg-gray-100 sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-32 xl:pb-64">
                <svg class="hidden lg:block absolute right-0 inset-y-0 h-full w-48 text-gray-100 transform translate-x-1/2" fill="currentColor" viewBox="0 0 100 100" preserveAspectRatio="none">
                    <polygon points="50,0 100,0 50,100 0,100" />
                </svg>
                <main class="mt-10 mx-auto max-w-screen-xl px-4 sm:mt-12 sm:px-6 md:mt-0 lg:px-8 lg:pt-32 xl:pt-64">
                    <div class="sm:text-center lg:text-left">
                        <h1 class="font-serif text-4xl tracking-tight leading-10 font-extrabold text-gray-900 sm:text-5xl sm:leading-none md:text-6xl">
                            Canadian Made
                            <br class="xl:hidden">
                            <span class="text-primary">Subscription Boxes</span>
                        </h1>
                        <p class="mt-3 text-base text-gray-600 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                            Canada needs its own subscription box, and it needs to be filled with Canadian made products.
                        </p>
                        <div class="mt-5 sm:mt-8 sm:flex sm:justify-center lg:justify-start">
                            <x-btn type="primary" href="{{ route('subscribe') }}" class="mr-3">Purchase Now</x-btn>
                            <x-btn type="link" href="{{ route('about') }}">About Us</x-btn>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <div class="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
            <img class="h-56 w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full" src="{{ asset('assets/dandelion-on-the-prairie-subscription-box.jpg') }}" alt="{{ config('app.name') }}">
        </div>
    </div>

    <x-photo-background photo="{{ asset('assets/field.jpg') }}">
        <x-container>
            <div class="md:max-w-lg md:mx-auto bg-gray-100 bg-opacity-75 p-10 rounded shadow-md">
                <div class="text-center">
                    <x-h2>Subscribe to our Newsletter!</x-h2>
                    <x-p>Stay up to date with all {{ config('app.name') }} news!</x-p>
                </div>
                <x-mail-chimp></x-mail-chimp>
            </div>
        </x-container>
    </x-photo-background>

    <x-content-with-photo photo="{{ asset('assets/canada2.jpg') }}">
        <div data-aos="fade-up">
            <x-h2><strong class='text-primary'>Proudly</strong> Canadian 🇨🇦</x-h2>
        </div>
        <div data-aos="fade-up">
            <x-p>{{ config('app.name') }} is a company that curates Canadian made products. We aim to find ethically made, sustainably made or naturually made items to put in the box. Here we believe that we should support other Canadian companies who also have the same views.</x-p>
        </div>
        <div data-aos="fade-up">
            <x-p>Supporting Canadian businesses we can contribute back to our own economy and support each other. By choosing cautiously made products we can also support a better and healthier Earth for our kids, grandkids and future generations.</x-p>
        </div>
        <div data-aos="fade-up">
            <x-p>Helping you make small changes that can lead to bigger changes. By helping these Canadian businesses we also become aware as consumers about where our products come from and how they are made.</x-p>
        </div>
    </x-content-with-photo>

    <x-content>
        <x-container>
            <x-row>
                <div class="md:w-1/2">
                    <div class="pr-4">
                        <div data-aos="fade">
                            <x-img src="{{ asset('assets/dandelion-on-the-prairie-box.jpg') }}" />
                        </div>
                    </div>
                </div>
                <div class="md:w-1/2 md:flex items-center">
                    <div class="p-4 lg:p-8">
                        <div data-aos="fade" data-aos-delay="100">
                            <x-h2>What's <strong class="text-primary">in</strong> the box?</x-h2>
                            <x-p>The box features 3-5 Canadian made products sent to your door each month! Sustainably, locally sourced. We designed our boxes to feature practical and fun products you can use every day.</x-p>
                            <x-p>Curious about past boxes?</x-p>
                            <x-btn type="primary" href="{{ route('subscribe') }}#past-boxes">View Past Boxes</x-btn>
                        </div>
                    </div>
                </div>
            </x-row>
        </x-container>
    </x-content>

    <x-content class="bg-gray-100">
        <x-container>
            <div class="text-center mb-4 lg:mb-12">
                <div data-aos="fade-up">
                    <x-h2>How it <span class="text-primary">Works</span></x-h2>
                </div>
            </div>
            <x-row>
                <div class="mb-10 md:mb-0 md:l-1/3">
                    <div class="px-4 text-center" data-aos="fade-up">
                        <div class="mb-4">
                            <x-img src="{{ asset('assets/candles.jpg') }}" />
                        </div>
                        <x-h3>1.</x-h3>
                        <x-p>Purchase a monthly subscription to the box.</x-p>
                    </div>
                </div>
                <div class="mb-10 md:mb-0 md:l-1/3">
                    <div class="px-4 text-center" data-aos="fade-up" data-aos-delay="100">
                        <div class="mb-4">
                            <x-img src="{{ asset('assets/new-moon-tea.jpg') }}" />
                        </div>
                        <x-h3>2.</x-h3>
                        <x-p>Wait until the box arrives at your door.</x-p>
                    </div>
                </div>
                <div class="mb-10 md:mb-0 md:l-1/3">
                    <div class="px-4 text-center" data-aos="fade-up" data-aos-delay="200">
                        <div class="mb-4">
                            <x-img src="{{ asset('assets/dandelion-on-the-prairie-subscription-box.jpg') }}" />
                        </div>
                        <x-h3>3.</x-h3>
                        <x-p>Enjoy beautifully hand-crafted, Canadian made goods!</x-p>
                    </div>
                </div>
            </x-row>
        </x-container>
    </x-content>

    @if(config('app.single_box_enabled'))
        <x-content>
            <x-container>
                <x-h2>Don't want to subscribe?</x-h2>
                <x-p>That is okay too! We do offer the <strong>Dandelion Floret box</strong> for people who want to spoil somebody (or themselves!) without subscribing monthly.</x-p>
                <x-btn type="primary" href="{{ route('subscribe') }}#floret" class="mr-3">Buy The Floret Box</x-btn>
            </x-container>
        </x-content>
    @endif

    <x-content class="bg-gray-200">
        <x-container>
            <div id="faqs">
                <x-h2>Frequently Asked Questions</x-h2>
                <faqs />
            </div>
        </x-container>
    </x-content>

    <x-subscribe-to-email></x-subscribe-to-email>

@endsection
