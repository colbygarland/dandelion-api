@extends('layouts.app')
@section('title', 'Newsletter')
@section('description', 'Sign up to stay up to date with all ' . config('app.name') . 'news!')

@section('content')

    <x-content-with-photo photo="{{ asset('assets/field.jpg') }}">
        <div class="mb-6 lg:mb-10">
            <x-h1><span class="font-bold">Subscribe to our <span class="text-primary">Newsletter</span></span></x-h1>
            <x-p>Stay up to date with all {{ config('app.name') }} news!</x-p>
        </div>
        <x-mail-chimp></x-mail-chimp>
    </x-content-with-photo>

@endsection
