@extends('layouts.app')
@section('title', 'Contact')
@section('description', 'Contact' . config('app.name') . ' for more information or help with your subscription.')

@section('content')

    <x-content>
        <x-container>
            <x-h1>Contact {{ config('app.name') }}</x-h1>
            <x-p>Have a question that needs answering? Get in touch.</x-p>
        </x-container>
    </x-content>

    <div class="block lg:flex relative">
        <div class="bg-gray-100 py-10 lg:py-20 xl:py-64 lg:w-1/2">
            <x-container>
                @if(Session::has('success'))
                    <div class="mb-6">
                        <x-alert type="success">{{ Session::get('success') }}</x-alert>
                    </div>
                @endif
                <x-h2>Send Us A Message</x-h2>
                <form method="post" action="{{ route('contact_form') }}" id="contact=form">
                    @csrf
                    <div class="mb-6">
                        <x-label>Name <span class="text-red-600">*</span></x-label>
                        <x-input name="name" required="true" />
                    </div>
                    <div class="mb-6">
                        <x-label>Email <span class="text-red-600">*</span></x-label>
                        <x-input name="email" type="email" required="true" />
                    </div>
                    <div class="mb-6">
                        <x-label>Subject <span class="text-red-600">*</span></x-label>
                        <x-select name="subject" required="true">
                            @foreach($subjects as $subject)
                                <option>{{ $subject }}</option>
                            @endforeach
                        </x-select>
                    </div>
                    <div class="mb-6">
                        <x-label>Comment <span class="text-red-600">*</span></x-label>
                        <x-textarea name="comments" required="true" />
                    </div>
                    <div class="mb-6">
                        <x-captcha></x-captcha>
                    </div>
                    <div class="mb-6">
                        <x-btn disabled id="submit" type="submit">Submit</x-btn>
                    </div>
                </form>
            </x-container>
        </div>
        <div class="hidden lg:block w-1/2 relative">
            <div style="background-image:url({{ asset('assets/field.jpg') }})" class="bg-cover bg-no-repeat bg-center hidden lg:block absolute h-full w-full"></div>
        </div>
    </div>

    <x-subscribe-to-email></x-subscribe-to-email>

@endsection
