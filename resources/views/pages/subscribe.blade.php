@extends('layouts.app')
@section('title', 'Buy')

@section('content')

    <x-content id="field">
        <x-container>
            <x-row>
                <div class="md:w-1/2">
                    <div class="p-6">
                        <x-img src="{{ asset('assets/dandelion-on-the-prairie-box.jpg') }}" />
                    </div>
                </div>
                <div class="md:w-1/2 md:flex items-center">
                    <div class="p-4 lg:p-8">
                        <x-h2>Dandelion Field Box</x-h2>
                        <x-p>Inside each box, you will find 3-5 items that are made in Canada. Comes once a month, delivered to your door.</x-p>
                        <x-ul>
                            <x-li><strong>$49.99 CAD each</strong> month.</x-li>
                            <x-li>Auto-renews each month, unless cancelled.</x-li>
                            <x-li>Billed on the 1st of <strong>each</strong> month.</x-li>
                            <x-li>Shipped out on the 15th of <strong>each</strong> month.</x-li>
                            <x-li><em>Due to COVID-19, shipping may be delayed.</em></x-li>
                        </x-ul>
                        @if(config('app.field_purchase_enabled'))
                            @if($subscriptions < config('app.max_subscriptions'))
                                @if(Auth::check() && Auth::user()->subscribed('Monthly'))
                                    <x-btn type="primary" href="{{ route('portal') }}">View Account</x-btn>
                                @else 
                                    <x-btn type="primary" href="{{ route('buy') }}">Order Now</x-btn>
                                @endif
                            @else
                                <x-alert type="info">
                                    Looks like we are all sold out for the upcoming month! You can sign up for next month, and you will be charged the beginning of next month.
                                </x-alert>
                                <x-p>Please sign up for the upcoming month below.</x-p>
                                <x-btn type="primary" href="{{ route('login') }}">Sign up for upcoming month</x-btn>
                            @endif
                        @endif
                    </div>
                </div>
            </x-row>
        </x-container>
    </x-content>

    @if(config('app.single_box_enabled'))
        <x-content class="bg-gray-200" id="floret">
            <x-container>
                <x-row>
                    <div class="md:w-1/2">
                        <div class="p-6">
                            <x-img src="{{ asset('assets/dandelion-on-the-prairie-box.jpg') }}" />
                        </div>
                    </div>
                    <div class="md:w-1/2 md:flex items-center">
                        <div class="p-4 lg:p-8">
                            <x-h2>Dandelion Floret Box</x-h2>
                            <x-p>Inside this carefully curated box, you will find 3-5 items that are made in Canada. Perfect for gifts, or spoiling yourself!</x-p>
                            @if(App\Http\Controllers\CashierController::isFloretOnSale())
                                <div class="px-6 py-2 mb-4  bg-red-500 text-white inline-block shadow">
                                    <p>{{ App\Http\Controllers\CashierController::SALE_DISCOUNT * 100 }}% off</p>
                                </div>
                            @endif
                            @if($count_single_boxes > 0)
                                <x-ul>
                                    @if(App\Http\Controllers\CashierController::isFloretOnSale())
                                        <x-li>One time purchase of <del><strong>${{ App\Http\Controllers\CashierController::REGULAR_SINGLE_BOX_PRICE }}</strong></del> <strong>${{ App\Http\Controllers\CashierController::getSalesPrice(true) }} CAD</strong></x-li>
                                    @else 
                                        <x-li>One time purchase of <strong>${{ App\Http\Controllers\CashierController::REGULAR_SINGLE_BOX_PRICE }} CAD</strong></x-li>
                                    @endif
                                    <x-li>Ships right to your door</x-li>
                                    <x-li>Ships out on the 15th of each month, or the next business day after the 15th</x-li>
                                    <x-li><em>Due to COVID-19, shipping may be delayed.</em></x-li>
                                </x-ul>
                            @endif
                            @if(config('app.floret_purchase_enabled'))
                                @if($count_single_boxes > 0)
                                    <x-btn type="primary" href="{{ route('buy_single_box') }}">Buy Now</x-btn>
                                @else
                                    <x-alert type="info">
                                        Looks like we are all sold out of individual boxes! You can sign up for our subscription box, and you will be charged the beginning of next month.
                                    </x-alert>
                                @endif
                            @endif
                        </div>
                    </div>
                </x-row>          
            </x-container>
        </x-content>
    @endif

    <x-content class="bg-gray-100">
        <x-container>
            <x-h2>What's in the box?</x-h2>
            <x-h3><span class="text-primary">Locally sourced, Canadian made products.</span></x-h3>
            <x-p>Inside each box, you will find 3-5 items that are made in Canada. {{ config('app.name') }} believes in supporting our own Canadian economy, buying from small business owners all over our great country.</x-p>
        </x-container>
    </x-content>

    <x-content id="past-boxes">
        <x-container>
            <x-h2>Past Boxes</x-h2>
            <x-h3><span class="text-primary">Check out some boxes we have had in the past!</span></x-h3>
            <section class="mt-12 lg:mt-24 text-center grid grid-cols-2 gap-4 lg:gap-16">
                <x-past-box img="{{ asset('assets/past-boxes/october-2020.jpg') }}" title="October 2020" />
                <x-past-box img="{{ asset('assets/past-boxes/november-2020.jpg') }}" title="November 2020" />
                <x-past-box img="{{ asset('assets/past-boxes/december-2020.jpg') }}" title="December 2020" />
                <x-past-box img="{{ asset('assets/past-boxes/january-2021.jpg') }}" title="January 2021" />
                <x-past-box img="{{ asset('assets/past-boxes/february-2021.jpg') }}" title="February 2021" />
            </section>
        </x-container>
    </x-content>

    <x-content class="bg-gray-200">
        <x-container>
            <div id="faqs">
                <x-h2>Frequently Asked Questions</x-h2>
                <faqs />
            </div>
        </x-container>
    </x-content>

    <x-subscribe-to-email></x-subscribe-to-email>

@endsection
