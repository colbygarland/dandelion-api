@extends('layouts.app')
@section('title', 'Users')

@section('content')

    <x-content>
        <x-container>
            <x-h1>Users</x-h1>
            <div class="block w-full overflow-x-auto relative">
                <table class="mt-5 border-collapse table-auto bg-white table-striped">
                    <thead>
                    <tr>
                        @foreach($head as $h)
                            <th class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                                {{ $h }}
                            </th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $row)
                        <tr class="">
                            @foreach($head as $col)
                                <td class="border-dashed border-t border-gray-200">
                                    <span class="text-gray-700 px-6 py-3 flex items-center">
                                        {{ $row[$col] }}
                                    </span>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </x-container>
    </x-content>

@endsection
