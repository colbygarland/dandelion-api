@extends('layouts.app')
@section('title', 'Floret Box Settings')

@section('content')

    <x-content>
        <x-container>
          <div class="mb-6">
            <x-h1>Floret Box Settings</x-h1>
            <x-label>Number of Floret Boxes currently: {{ $numberOfBoxes }}</x-label>
          </div>
          <x-h2>Update Number of Floret Boxes</x-h2>
          @if(Session::has('success'))
            <div class="mb-6">
              <x-alert type="success">{{ Session::get('success') }}</x-alert>
            </div>
          @endif
          <form method="POST" action="{{ route('floret_form') }}">
            @csrf
            <div class="mb-6">
              <x-label>Number of boxes currently</x-label>
              <x-input name="number" type="number" required="true" placeholder="Enter number of boxes" />
            </div>
            <div class="mb-6">
              <x-btn type="submit">Submit</x-btn>
            </div>
          </form>
        </x-container>
    </x-content>

    <x-content>
      <x-container>
        
      </x-container>
    </x-content>

@endsection
