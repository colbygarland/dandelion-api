@extends('layouts.app')
@section('title', 'About')
@section('description', 'Dandelion on the Prarie is a monthly subscription box filled with Canadian made products.')

@section('content')

    <x-content class="bg-gray-100">
        <x-container>
            <x-h1>About {{ config('app.name') }}</x-h1>
        </x-container>
    </x-content>

    <div class="block lg:flex relative">
        <div class="hidden lg:block w-1/2 relative">
            <div style="background-image:url({{ asset('assets/brittany.jpg') }})" class="bg-cover bg-no-repeat bg-center hidden lg:block absolute h-full w-full"></div>
        </div>
        <div class="bg-gray-100 py-10 lg:py-20 lg:w-1/2">
            <x-container>
                <x-p>I am a busy mother of four beautiful children. We live in a small, small, small, northern Alberta community. Both my husband and I, our parents, and our grandparents, call this home. My husband and I have known each other since grade two. We graduated together, along with 10 other students in our class.</x-p>
                <x-p>Moose, bear, fox, and other wildlife are constantly in our back yard and they love my chickens! Ah, life on the prairie! I've always loved wildflowers. The one, that is always in my yard, is the famous, bright yellow dandelion. My brother in-law has nicknamed me, Little House on the Prairie, for a number of valid reasons!</x-p>
                <x-p>I recently had an idea! I believe that Canada needs its own subscription box, filled with Canadian made products! An idea was born and a name finally came to me; {{ config('app.name') }}. Here, I will try to source Canadian made products that are cautiously made or natural. Three to five items will be shipped to you monthly. Your wait time will be minimal!</x-p>
                <x-p>I, along with my tech savvy brother in-law, Colby, would like to welcome you to the {{ config('app.name') }} family.</x-p>
                <div>
                    <img class="w-40" src="{{ asset('assets/brittany-sig.png') }}" alt="" />
                </div>
                <div class="lg:hidden">
                    <img class="block mx-auto" src="{{ asset('assets/brittany.jpg') }}" alt="Field" />
                </div>
            </x-container>
        </div>
    </div>

    <x-content>
        <x-container>
            <x-h2>Supporting Canadian Business 🇨🇦</x-h2>
            <x-h4><span class="text-primary">Big and Small, Across the Country</span></x-h4>
            <x-p>We believe in supporting local. And by local, we mean Canada 😁  {{ config('app.name') }} supplies boxes with quality, made in Canada products made all over our beautiful country.</x-p>
        </x-container>
    </x-content>

    <x-content class="bg-gray-100">
        <x-container>
            <div id="faqs">
                <x-h2>Frequently Asked Questions</x-h2>
                <faqs />
            </div>
        </x-container>
    </x-content>

    <x-subscribe-to-email></x-subscribe-to-email>

@endsection
