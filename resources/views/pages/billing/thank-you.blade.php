@extends('layouts.app')
@section('title', 'Thank You')

@section('content')

    <x-content-with-photo photo="{{ asset('assets/field.jpg') }}">
        <x-h1>Thank you!</x-h1>
        <x-h3>Your purchase has been confirmed.</x-h3>
        <x-p>Look forward to a box full of Canadian made products coming your way, each month!</x-p>
      
        <div class="flex">
            <x-btn type="primary" href="{{ route('portal') }}">View Account</x-btn>
            <x-btn class="ml-3" type="link" href="{{ route('subscribe') }}#faqs">Questions?</x-btn>
        </div>

        <x-h3>How did you hear about us?</x-h3>
        <survey />

    </x-content-with-photo>

@endsection
