@extends('layouts.billing.app')
@section('title', 'Secure Checkout | Purchase Single Box')

@section('content')
    <x-content>
        <x-container>
            <stripe-single-box
                stripe-secret="{{ Auth::check() ? Auth::user()->createSetupIntent()->client_secret : '' }}"
                stripe-key="{{ config('app.stripe_public_key') }}"
                user-email="{{ Auth::check() ? Auth::user()->email : '' }}"
                single-box-price="{{ App\Http\Controllers\CashierController::REGULAR_SINGLE_BOX_PRICE }}"
                percentage-off="{{ App\Http\Controllers\CashierController::SALE_DISCOUNT * 100 }}"
                sale-box-price="{{ App\Http\Controllers\CashierController::getSalesPrice(true) }}"
                on-sale="{{ App\Http\Controllers\CashierController::isFloretOnSale() }}"
                max-quantity="{{ App\Http\Controllers\CashierController::getSingleBoxCount() }}"
            ></stripe-single-box>
        </x-container>
    </x-content>
@endsection
