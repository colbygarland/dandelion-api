@extends('layouts.billing.app')
@section('title', 'Secure Checkout')

@section('content')
    <x-content>
        <x-container>
            <stripe
                stripe-secret="{{ Auth::check() ? Auth::user()->createSetupIntent()->client_secret : '' }}"
                stripe-key="{{ config('app.stripe_public_key') }}"
                user-email="{{ Auth::check() ? Auth::user()->email : '' }}"
            ></stripe>
        </x-container>
    </x-content>
@endsection
