<div
    class="g-recaptcha"
    data-sitekey="{{ config('app.captcha_site_key') }}"
    data-callback="enable"
></div>
