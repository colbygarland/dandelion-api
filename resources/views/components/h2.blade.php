<h2 class="font-serif font-semibold text-lg lg:text-4xl mb-6 inline-block border-b-2 text-gray-800">
    {{ $slot }}
</h2>
