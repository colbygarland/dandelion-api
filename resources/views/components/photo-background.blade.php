<div class="@if(! is_null($slant)) photo-background @endif relative bg-cover bg-no-repeat bg-center text-center lg:flex flex-col items-center justify-center py-20 px-4 lg:px-16 lg:py-20 @if(! is_null($class)) {{ $class }} @endif" style="background-image:url({{ $photo }})">
    {{ $slot }}
</div>
