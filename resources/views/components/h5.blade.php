<h5 class="text-lg lg:text-xl mb-6 text-gray-800">
    {{ $slot }}
</h5>
