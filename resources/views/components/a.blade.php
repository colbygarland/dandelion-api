<a
    href="{{ $href }}"
    title="{{ $slot }}"
    @if(! empty($target)) target="{{ $target }}" @endif

    class="
    font-bold
    text-primary
    hover:text-secondary
    @if(! empty($class)) {{ $class }} @endif
    "
>
    {{ $slot }}
</a>
