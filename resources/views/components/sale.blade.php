<div class="bg-red-600 text-white">
  <div class="text-center">
      <p><a href="{{ route('subscribe') }}#floret" class="block px-6 py-3">Flash sale! Floret boxes are now {{ App\Http\Controllers\CashierController::SALE_DISCOUNT * 100}}% off! Click here to purchase</a></p>
  </div>
</div>
