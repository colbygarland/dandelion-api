@component('components.buttons._button-base')
    @slot('url') {{ $url }} @endslot
    @slot('classes') bg-none border-white text-white border-2 hover:bg-white hover:text-black @endslot
    {{ $slot }}
@endcomponent
