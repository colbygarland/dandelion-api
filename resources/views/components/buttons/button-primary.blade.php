@component('components.buttons._button-base')
    @slot('url') {{ $url }} @endslot
    @slot('classes') bg-primary hover:bg-secondary @endslot
    {{ $slot }}
@endcomponent
