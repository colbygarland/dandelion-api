<a href="{{ $url }}" style="text-decoration:none!important">
    <span class="text-white block py-2 px-4 font-bold no-underline rounded {{ $classes }}">
        {{ $slot }}
    </span>
</a>
