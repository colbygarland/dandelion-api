<div class="block lg:flex relative">
    <div class="bg-gray-100 py-10 lg:py-20 xl:py-32 lg:w-1/2">
        <x-container>
            {{ $slot }}
            <div class="lg:hidden">
                <img class="block mx-auto" src="{{ $photo }}" alt="{{ config('app.name') }}" />
            </div>
        </x-container>
    </div>
    <div class="hidden lg:block w-1/2 relative">
        <div style="background-image:url({{ $photo }})" class="bg-cover bg-no-repeat bg-center hidden lg:block absolute h-full w-full"></div>
    </div>
</div>
