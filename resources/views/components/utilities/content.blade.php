<div class="py-10 md:py-12 lg:py-20 {{ $classes ?? '' }}">
    {{ $slot }}
</div>
