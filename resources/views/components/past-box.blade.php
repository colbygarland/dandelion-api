@if($visible)
    <div>
        <img loading="lazy" class="w-full h-64 lg:h-80 object-cover shadow-md" src="{{ $img }}" alt="Dandelion on the Prariie - {{ $title }}" />
        <p class="mt-4 font-bold">{{ $title }}</p>
    </div>
@endif
