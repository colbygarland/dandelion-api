<textarea
    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
    id="{{ $id }}"
    name="{{ $name }}"
    placeholder="{{ $placeholder }}"

    @if($required) required @endif
>@if(! empty($value)) {{ $value }} @endif</textarea>
