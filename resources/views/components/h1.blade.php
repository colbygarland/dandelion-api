<h1 class="font-serif font-semibold text-2xl md:text-3xl lg:text-4xl xl:text-5xl text-gray-700">
    {{ $slot }}
</h1>
