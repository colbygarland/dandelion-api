<div class="rounded overflow-hidden shadow bg-white mb-6 lg:mb-10">
    <div class="px-6 py-4">
        @if(! empty($title))<h4 class="font-bold mb-4">{{ $title }}</h4>@endif
        {{ $slot }}
    </div>
</div>
