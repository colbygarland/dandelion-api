<p class="mb-3 leading-loose text-gray-700">
    @if(! empty($class))
        <span class="{{ $class }}">{!! $slot !!}</span>
    @else
        {!! $slot !!}
    @endif
</p>
