<x-photo-background photo="{{ asset('assets/bee-on-dandelion.jpg') }}">
        <div class="md:max-w-lg md:mx-auto bg-gray-100 bg-opacity-75 p-10 rounded shadow-md">
            <div class="text-center">
                <x-h2 underline="false">Subscribe to our Newsletter!</x-h2>
                <x-p><span class="text-shadow">Don't miss out on any {{ config('app.name') }} news!</span></x-p>
        <x-btn type="white" href="{{ route('newsletter') }}">Subscribe</x-btn>
            </div>
        </div>
</x-photo-background>
