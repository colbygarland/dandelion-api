<div class="transition transform fixed z-100 bottom-0 inset-x-0 pb-2 sm:pb-5 opacity-100 scale-100 translate-y-0 ease-out duration-500">
    <div class="max-w-screen-xl mx-auto px-2 sm:px-4">
        <div class="p-2 rounded-lg bg-primary shadow-lg sm:p-3">
            <div class="flex items-center justify-between flex-wrap">
                <div class="w-0 flex-1 flex items-center">
                    <img class="h-12" src="{{ asset('android-chrome-192x192.png') }}" alt="{{ config('app.name') }}" />
                    <p class="ml-3 font-medium text-white">
                        <span class="lg:hidden">
                            {{ $title }}
                        </span>
                        <span class="hidden lg:inline text-white">
                            <strong class="text-white font-semibold mr-1">{{ $title }}</strong>
                            <span class="xl:hidden">{{ $desc }}</span>
                            <span class="hidden xl:inline">{{ $desc }}</span>
                        </span>
                    </p>
                </div>
                <div class="order-2 mt-2 flex-shrink-0 w-full sm:order-2 sm:mt-0 sm:w-auto">
                    <div class="rounded-md shadow-sm">
                        <a href="{{ $link }}" class="flex items-center justify-center px-4 py-2 border border-transparent text-sm leading-5 font-medium rounded-md text-gray-900 bg-white hover:text-gray-800 focus:outline-none focus:underline">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>