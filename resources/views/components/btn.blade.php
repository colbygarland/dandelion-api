@php
$default_classes = 'inline-block font-bold px-5 py-3 rounded mb-8 transition-all duration-200 ease-in-out';
@endphp

@if($type == 'submit')
    <button @if($disabled) disabled @endif id="{{ $id }}" type="submit" class="{{ $default_classes }} bg-primary hover:bg-secondary text-white">{{ $slot }}</button>
@else
    <a
    @if($disabled) disabled @endif
    id="{{ $id }}"
    href="{{ $href }}"
    title="{{ $slot }}"
    class="
    @if(! empty($class)) {{ $class }} @endif
    {{ $default_classes }}
    @switch($type)
        @case('primary')
            bg-primary
            hover:bg-secondary
            text-white
        @break
        @case('ghost-primary')
            bg-none
            border-primary
            border-2
            text-primary
            hover:bg-primary
            hover:text-white
        @break
        @case('link')
            text-primary
            hover:bg-primary
            hover:text-white
        @break
        @case('secondary')
            bg-secondary
            hover:bg-primary
            text-white
        @break
        @case('ghost-secondary')
            bg-none
            border-secondary
            border-2
            text-secondary
            hover:bg-secondary
            hover:text-white
        @break
        @case('ghost-white')
            bg-none
            border-white
            border-2
            text-white
            hover:bg-white
            hover:text-black
        @break
        @case('white')
            bg-white
            text-gray-800
            hover:bg-primary
            hover:text-white
        @break
    @endswitch
">
    {{ $slot }}
</a>

@endif
