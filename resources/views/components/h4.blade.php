<h4 class="font-semibold text-lg lg:text-2xl mb-6 text-gray-800">
    {{ $slot }}
</h4>
