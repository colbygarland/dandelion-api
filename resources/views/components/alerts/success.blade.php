@component('components.alerts._base')
    @slot('classes') bg-green-100 border-green-400 text-green-700 @endslot
    {{ $slot }}
@endcomponent
