@component('components.alerts._base')
    @slot('classes') bg-red-100 border-red-400 text-red-700 @endslot
    {{ $slot }}
@endcomponent
