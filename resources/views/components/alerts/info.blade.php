@component('components.alerts._base')
    @slot('classes') bg-blue-100 border-blue-400 text-blue-700 @endslot
    {{ $slot }}
@endcomponent
