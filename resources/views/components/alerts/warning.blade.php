@component('components.alerts._base')
    @slot('classes') bg-yellow-100 border-yellow-400 text-yellow-700 @endslot
    {{ $slot }}
@endcomponent
