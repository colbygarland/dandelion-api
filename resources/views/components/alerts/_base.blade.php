<div class=" border-2 px-4 py-3 rounded relative {{ $classes }}" role="alert">
    <span class="block sm:inline">{{ $slot }}</span>
</div>
