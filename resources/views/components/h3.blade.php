<h3 class="font-serif font-semibold text-lg lg:text-3xl mb-6 text-gray-800">
    {{ $slot }}
</h3>
