<div id="mc-embedded-subscribe-form">
    <form action="https://gmail.us10.list-manage.com/subscribe/post?u=ba4015e3fea11713d4549488f&amp;id=ade56fb97d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
            <div class="mb-6">
                <x-label for="mce-EMAIL">Email Address  <span class="text-red-600">*</span></x-label>
                <x-input placeholder="Email address" required="true" type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" />
            </div>
            <div class="mb-6">
                <x-label for="mce-FNAME">First Name </x-label>
                <x-input placeholder="First name" type="text" value="" name="FNAME" class="" id="mce-FNAME"/>
            </div>
            <div class="clear">
                <x-btn type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">Subscribe</x-btn>
            </div>
            <div class="indicates-required">
                <small><span class="text-red-600">*</span> indicates required</small>
            </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ba4015e3fea11713d4549488f_ade56fb97d" tabindex="-1" value=""></div>
        </div>
    </form>
</div>
