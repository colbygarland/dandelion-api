<table class="mt-5 border-collapse table-auto w-full whitespace-no-wrap bg-white table-striped relative">
    <thead>
    <tr>
        @foreach($head as $h)
            <th class="bg-gray-100 sticky top-0 border-b border-gray-200 px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">
                {{ $h }}
            </th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($body as $row)
        <tr>
           @foreach($head as $col)
               <td>{{ $row[$col] }}</td>
           @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
