<label class="block text-gray-700 text-sm font-bold mb-2 {{ $classes ?? '' }}" for="{{ $for ?? '' }}">
    {{ $slot }}
</label>
