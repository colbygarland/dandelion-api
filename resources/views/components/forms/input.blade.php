<input
    class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline{{ $classes ?? '' }}"
    id="{{ $id ?? null }}"
    type="{{ $type ?? 'text' }}"
    placeholder="{{ $placeholder ?? '' }}"
    name="{{ $name }}"

    @isset($required) required @endisset
    @isset($value) value="{{ $value }}" @endisset
/>
