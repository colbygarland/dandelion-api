<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Dandelion on the Prairie') }}</title>
    <meta property="og:image" content="https://dandelionontheprairie.ca/assets/social-2020-11-14.jpg"/>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />

    @if(App::environment('production'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174497170-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-174497170-1');
        </script>
    @endif

    <script src="https://js.stripe.com/v3/"></script>
</head>
<body>
<div id="app">

    <header class="py-4 shadow sticky bg-white z-10" style="top:0">
        <x-container>
            <h1 class="font-bold text-xl"><a href="{{ config('app.url') }}">{{ config('app.name') }}</a></h1>
        </x-container>
    </header>

    <main class="bg-gray-100">
        @yield('content')
    </main>

</div>
</body>
</html>
