<app-header
    v-bind:user="{{ Auth::check() ?? 0 ? 1 : 0 }}"
    v-bind:login="'{{ route('login') }}'"
    v-bind:register="'{{ route('register') }}'"
    v-bind:profile="'{{ route('portal') }}'"
    v-bind:logout="'{{ route('logout') }}'"
    v-bind:redirect="'{{ route('home') }}'"
    v-bind:base="'{{ config('app.url') }}'"
    v-bind:admin="{{ Auth::user()->is_admin ?? 0 ? 1 : 0 }}"
></app-header>
