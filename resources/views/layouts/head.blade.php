<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Dandelion on the Prairie') }}</title>

    @php 
    $socialShare = 'https://dandelionontheprairie.ca/assets/social-2020-11-14.jpg';
    @endphp

    <meta property="og:image" content="{{ $socialShare }}"/>
    <meta name="Description" content="@yield('description')">
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:url" content="{{ config('app.url') }}" >
    <meta property="og:title" content="{{ config('app.name', 'Dandelion on the Prairie') }}" >
    <meta property="fb:app_id" content="672740870012977" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:url" content="{{ config('app.url') }}"
    <meta name="twitter:title" content=">@yield('title') | {{ config('app.name', 'Dandelion on the Prairie') }}" />
    <meta name="twitter:description" content="@yield('description')" />

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#e5c93a">
    <meta name="theme-color" content="#e5c93a">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&family=Noto+Serif:wght@400;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />

    @if(App::environment('production'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174497170-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-174497170-1');
        </script>
    @endif

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function enable(){
            document.getElementById('submit').disabled = false;
        }
    </script>
</head>
