<footer class="p-3 bg-secondary text-white">
    <div class="container mx-auto px-6 prose max-w-none text-center">
        <p>
            <small class="text-white">
                &copy;{{ date('Y') }} {{ config('app.name') }}. All rights reserved<span class="hidden md:inline-block mx-2">&bull;</span> <br class="md:hidden" />
                <a href="{{ route('privacy-policy') }}">Privacy Policy</a><span class="hidden md:inline-block mx-2">&bull;</span> <br class="md:hidden" />
                <a href="{{ route('terms-of-service') }}">Terms of Service</a><span class="hidden md:inline-block mx-2">&bull;</span> <br class="md:hidden" />
                Website by <a href="https://colbygarland.ca/?utm_source=website&utm_medium=app&utm_campaign=dandelion" target="_blank">Colby Garland</a>
            </small>
        </p>
    </div>
</footer>

