<div class="px-6 py-3 bg-secondary text-white">
    <div class="container mx-auto text-center">
        <p><small>Due to Covid-19, our shipping times have been delayed. Thanks for your patience and for supporting Canadian businesses!</small></p>
    </div>
</div>