@include('layouts.head')

<body>
    <div id="app">

        @if(App\Http\Controllers\CashierController::isFloretOnSale())
            @include('components.sale')
        @endif

        @include('layouts.covid')
        @include('layouts.header')

        <main>
            @yield('content')
        </main>

        @include('layouts.footer')
    </div>
</body>
</html>
