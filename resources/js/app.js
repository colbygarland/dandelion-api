/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

export const formatDollar = number => {
    return new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(number);
};

Vue.component("menu-item", require("./components/MenuItem.vue").default);
Vue.component("menu-items", require("./components/Menu.vue").default);
Vue.component("app-header", require("./components/Header.vue").default);
Vue.component("stripe", require("./components/Stripe.vue").default);
Vue.component("social-media", require("./components/SocialMedia.vue").default);
Vue.component("faq", require("./components/FAQ.vue").default);
Vue.component("faqs", require("./components/FAQs.vue").default);
Vue.component("recaptcha", require("./components/Recaptcha.vue").default);
Vue.component("toggle-users", require("./components/ToggleUsers.vue").default);
Vue.component(
    "stripe-single-box",
    require("./components/StripeSingleBox.vue").default
);
Vue.component("survey", require("./components/Survey.vue").default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});
