<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address')->nullable()->after('remember_token');
            $table->string('address2')->nullable()->after('address');
            $table->string('country')->nullable()->after('address2');
            $table->string('postal_code')->nullable()->after('country');
            $table->string('city')->nullable()->after('postal_code');
            $table->string('province')->nullable()->after('city');
            $table->string('phone')->nullable()->after('province');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('address2');
            $table->dropColumn('country');
            $table->dropColumn('postal_code');
            $table->dropColumn('city');
            $table->dropColumn('province');
            $table->dropColumn('phone');
        });
    }
}
