<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insertOrIgnore([
                [
                    'name' => 'Colby Garland',
                    'email' => 'colbygarland@gmail.com',
                    'email_verified_at' => Carbon::now(),
                    'password' => Hash::make('typotypo'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'is_admin' => true,
                ],
                [
                    'name' => 'Brittany Dolen',
                    'email' => 'dandelionontheprairie@gmail.com',
                    'email_verified_at' => Carbon::now(),
                    'password' => Hash::make('Dandeli0nPrairie!'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'is_admin' => true,
                ],
            ]);
    }
}
