module.exports = {
    root: true,
    extends: ["eslint:recommended", "airbnb", "airbnb/hooks"],
    env: {
        "jest/globals": true
    },
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint", "jest"],
    rules: {
        "comma-dangle": ["warn", "always-multiline"],
        "import/prefer-default-export": "off",
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                js: "never",
                jsx: "never",
                ts: "never",
                tsx: "never"
            }
        ],
        "lines-between-class-members": "never",
        "max-len": {
            code: 160
        }
    },
    settings: {
        "import/resolver": {
            node: {
                extensions: [".js", ".jsx", ".ts", ".tsx"],
                moduleDirectory: ["node_modules", "src/"]
            }
        }
    }
};
